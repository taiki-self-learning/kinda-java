
// -- 画面遷移 --
// -- ログイン画面 --
Cypress.Commands.add('login', () => {
  cy.visit('login')
  cy.url().should('include', '/login')
});

// -- ユーザ指定ログイン処理 --
Cypress.Commands.add('doLogin', (id, pass) => {
  cy.login();
  cy.get('#id').type(id);
  cy.get('#password').type(pass);
  cy.get('#login').click();
});

// -- 権限指定ログイン --
Cypress.Commands.add('doTypeLogin', (roleType) => {

  // 権限タイプ
  const types = {
    // 管理者権限
    admin: {
      id: '999999',
      pass: 'admin'
    },
    // ユーザ権限
    user: {
      id: '000000',
      pass: 'user'
    }
  };

  const role = types[roleType];

  cy.login();
  cy.doLogin(role.id, role.pass);
});