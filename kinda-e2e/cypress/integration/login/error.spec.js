describe('ログイン画面(異常系)', () => {
  it('認証失敗し、ログイン画面に遷移すること', () => {
    // execute
    cy.doLogin('111111', 'test');

    // assert
    cy.url().should('include', '/login');
    cy.get('body > div > div.alert.alert-danger.alert-dismissible > span')
      .should('have.text', '入力されたIDやパスワードが正しくありません。');
  });
});