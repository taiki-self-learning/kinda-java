describe('ログイン画面(正常系)', () => {
  it('管理者権限でログインし、ホーム画面に遷移すること', () => {
    // execute
    cy.doTypeLogin('admin');

    // assert
    cy.url().should('include', '/home');
    cy.get('#header > div > div.collapse.navbar-collapse.justify-content-end > ul > li:nth-child(1) > a > i')
      .should('have.text', 'how_to_reg');
    cy.get('#header > div > div.collapse.navbar-collapse.justify-content-end > ul > li:nth-child(1) > a > span')
      .should('have.text', 'あどみん 太郎');
  });
  it('ユーザ権限でログインし、ホーム画面に遷移すること', () => {
    // execute
    cy.doTypeLogin('user');

    // assert
    cy.url().should('include', '/home');
    cy.get('#header > div > div.collapse.navbar-collapse.justify-content-end > ul > li:nth-child(1) > a > i')
      .should('have.text', 'person');
    cy.get('#header > div > div.collapse.navbar-collapse.justify-content-end > ul > li:nth-child(1) > a > span')
      .should('have.text', 'ゆーざ 次郎');
  });
});