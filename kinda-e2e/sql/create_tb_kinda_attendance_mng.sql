create table tb_kinda_attendance_mng (
  staff_id character(6) not null
  , attendance_date date not null
  , attendance_div character(3) not null
  , attendance_div_detail character(3)
  , punch_in_time time
  , punch_out_time time
  , rest_time integer
  , attendance_reason varchar(255)
  , created_at timestamp not null
  , create_user varchar(255) not null
  , updated_at timestamp not null
  , update_user varchar(255) not null
  , primary key (staff_id, attendance_date)
);