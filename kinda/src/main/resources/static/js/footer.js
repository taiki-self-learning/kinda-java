window.addEventListener('DOMContentLoaded', (event) => {
  const footer = new Vue ({
    el: '#footer',
    data: function() {
      return {
        isSmartPhone: isSmartPhone()
      }
    },
    methods: {
      toHome: function() {
        window.location.href = "/kinda/home";
      },
      toDetail: function() {
        window.location.href = "/kinda/detail";
      },
      toSchedule: function() {
        window.location.href = "/kinda/schedule";
      },
      toSettings: function() {
        window.location.href = "/kinda/settings";
      }
    }
  })
});