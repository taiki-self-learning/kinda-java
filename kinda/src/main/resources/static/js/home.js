window.addEventListener('DOMContentLoaded', (event) => {
  const home = new Vue ({
    el: '#home',
    data: function() {
      return {
        attendanceInfo: attendanceInfo,
        isSmartPhone: isSmartPhone(),
        today: "",
        hourBefore: "static/img/0.png",
        hourAfter: "static/img/0.png",
        minBefore: "static/img/0.png",
        minAfter: "static/img/0.png",
        secBefore: "static/img/0.png",
        secAfter: "static/img/0.png",
        colon: "static/img/colon.png",
        btnPunchIn: {
          disabled: false
        },
        btnPunchOut: {
          disabled: false
        }
      }
    },
    mounted: function() {
      this.btnPunchAbleCtrl();
      this.setDay();
      this.setClock();
      setInterval(this.setClock, 500);
    },
    methods: {
      setDay: function() {
        const today = new Date();
        const year = today.getFullYear();
        const month = today.getMonth() + 1;
        const date = today.getDate();
        const dayOfWeek = [ '日', '月', '火', '水', '木', '金', '土' ][today.getDay()];
        this.today = year + '年' + month + '月' + date + '日' + '(' + dayOfWeek + ')';
      },
      setClock: function() {
        const now = new Date();
        const hour = this.alignTwoDigits(now.getHours());
        const min = this.alignTwoDigits(now.getMinutes());
        const sec = this.alignTwoDigits(now.getSeconds());
        this.hourBefore = 'static/img/' + hour.charAt(0) + '.png';
        this.hourAfter = 'static/img/' + hour.charAt(1) + '.png';
        this.minBefore = 'static/img/' + min.charAt(0) + '.png';
        this.minAfter = 'static/img/' + min.charAt(1) + '.png';
        this.secBefore = 'static/img/' + sec.charAt(0) + '.png';
        this.secAfter = 'static/img/' + sec.charAt(1) + '.png';
      },
      alignTwoDigits: function(target) {
        target = '0' + target;
        return target.substring(target.length-2, target.length);
      },
      onClickPunchIn: function() {
        const reqJson = {
            operateDiv: 'C',
            attendancePayloadList: [
                {
                    staffId: null,
                    attendanceDate: this.getDay(),
                    attendanceDiv: '001',
                    attendanceDivDetail: null,
                    punchInTime: this.getNow(),
                    punchOutTime: null,
                    restTime: 0,
                    attendanceReason: null
                }
            ]
        };
        this.doPostPunch(reqJson);
      },
      onClickPunchOut: function() {
        const attendanceDivDetail = this.attendanceInfo.attendancePayloadList[0].attendanceDivDetail;
        const punchInTime = this.attendanceInfo.attendancePayloadList[0].punchInTime;
        const attendanceReason = this.attendanceInfo.attendancePayloadList[0].attendanceReason;
        reqJson = {
            operateDiv: 'U',
            attendancePayloadList: [
                {
                    attendanceDate: this.getDay(),
                    attendanceDiv: '004',
                    attendanceDivDetail: attendanceDivDetail,
                    punchInTime: punchInTime,
                    punchOutTime: this.getNow(),
                    restTime: 60,
                    attendanceReason: attendanceReason
                }
            ]
        };
        this.doPostPunch(reqJson);
      },
      getDay: function() {
        const now = new Date();
        const year = now.getFullYear();
        const month = now.getMonth() + 1;
        const date = now.getDate();
        return year + '-' + this.alignTwoDigits(month) + '-' + this.alignTwoDigits(date);
      },
      getNow: function() {
        const now = new Date();
        const hour = now.getHours();
        const min = now.getMinutes();
        const sec = now.getSeconds();
        return this.alignTwoDigits(hour) + ':' + this.alignTwoDigits(min) + ':' + this.alignTwoDigits(sec); 
      },
      doPostPunch: async function(reqJson) {
        const res = await axios.post('api/v1/attendance/operate', reqJson);
        this.attendanceInfo = res.data;
        this.btnPunchAbleCtrl();
      },
      btnPunchAbleCtrl: function() {
        let attendanceDiv = null;
        if (this.attendanceInfo != null) {
          attendanceDiv = this.attendanceInfo.attendancePayloadList[0].attendanceDiv;
        }
        if (attendanceDiv == null) {
          this.btnPunchIn.disabled = false;
          this.btnPunchOut.disabled = true;
        }else if (attendanceDiv == '001') {
          this.btnPunchIn.disabled = true;
          this.btnPunchOut.disabled = false;
        }else {
          this.btnPunchIn.disabled = true;
          this.btnPunchOut.disabled = true;
        }
      }
    }
  })
});