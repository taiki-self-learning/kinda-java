function isSmartPhone() {
  const userAgent = navigator.userAgent;
  return userAgent.indexOf('iPhone') > 0 || userAgent.indexOf('Android') > 0;
}