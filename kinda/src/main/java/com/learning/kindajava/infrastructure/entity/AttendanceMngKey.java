package com.learning.kindajava.infrastructure.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Embeddable
@Data
public class AttendanceMngKey implements Serializable {

    /** serialVersionUID  */
    private static final long serialVersionUID = 1L;

    /** 社員ID */
    @Column(name = "staff_id")
    private String staffId;

    /** 日付 */
    @Temporal(TemporalType.DATE)
    @Column(name = "attendance_date")
    private Date attendanceDate;
}