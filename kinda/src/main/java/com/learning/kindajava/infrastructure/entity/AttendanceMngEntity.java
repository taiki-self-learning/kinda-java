package com.learning.kindajava.infrastructure.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.learning.kindajava.domain.model.AttendanceInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tb_kinda_attendance_mng")
public class AttendanceMngEntity extends AbstractEntity {

    /**
     * コンストラクタ。
     */
    public AttendanceMngEntity() {
        this.primaryKey = new AttendanceMngKey();
    }

    /**
     * コンストラクタ。
     * 
     * @param request 勤怠情報モデルリクエスト
     */
    public AttendanceMngEntity(AttendanceInfo request) {
        this.primaryKey = new AttendanceMngKey(request.getStaffId(), request.getAttendanceDate());
        this.attendanceDiv = request.getAttendanceDiv();
        this.attendanceDivDetail = request.getAttendanceDivDetail();
        this.punchInTime = request.getPunchInTime();
        this.punchOutTime = request.getPunchOutTime();
        this.restTime = request.getRestTime();
        this.attendanceReason = request.getAttendanceReason();
        this.createUser = request.getStaffId();
        this.updateUser = request.getStaffId();
    }

    /** serialVersionUID  */
    private static final long serialVersionUID = 1L;

    /** 複合キー */
    @EmbeddedId
    private AttendanceMngKey primaryKey;

    /** 勤怠区分 */
    @Column(name = "attendance_div")
    private String attendanceDiv;

    /** 勤怠区分詳細 */
    @Column(name = "attendance_div_detail")
    private String attendanceDivDetail;

    /** 出勤時刻 */
    @Temporal(TemporalType.TIME)
    @Column(name = "punch_in_time")
    private Date punchInTime;

    /** 退勤時刻 */
    @Temporal(TemporalType.TIME)
    @Column(name = "punch_out_time")
    private Date punchOutTime;

    /** 休憩時間 */
    @Column(name = "rest_time")
    private int restTime;

    /** 勤怠理由 */
    @Column(name = "attendance_reason")
    private String attendanceReason;

    /**
     * 社員ID取得。
     * 
     * @return 社員ID
     */
    public String getStaffId() {
        return this.primaryKey.getStaffId();
    }

    /**
     * 日付取得。
     * 
     * @return 日付
     */
    public Date getAttendanceDate() {
        return this.primaryKey.getAttendanceDate();
    }
}