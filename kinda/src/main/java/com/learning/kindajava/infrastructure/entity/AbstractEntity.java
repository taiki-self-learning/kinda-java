package com.learning.kindajava.infrastructure.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.NoArgsConstructor;

/**
 * 共通エンティティクラス。
 */
@NoArgsConstructor
@MappedSuperclass
public class AbstractEntity implements Serializable {

    /** serialVersionUID  */
    private static final long serialVersionUID = 1L;

    /** 登録日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", updatable = false)
    private Date createdAt;

    /** 登録者 */
    @Column(name = "create_user", updatable = false)
    protected String createUser;

    /** 更新日 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    private Date updatedAt;

    /** 更新者 */
    @Column(name = "update_user")
    protected String updateUser;

    /**
     * 登録前処理
     */
    @PrePersist
    public void prePersist() {
        this.createdAt = new Date();
        this.updatedAt = new Date();
    }

    /**
     * 更新前処理
     */
    @PreUpdate
    public void preUpdate() {
        this.updatedAt = new Date();
    }
}
