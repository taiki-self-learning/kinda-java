package com.learning.kindajava.infrastructure.client;

import static com.learning.kindajava.common.Constant.ResultCd.WARNING;

import com.learning.kindajava.common.configuration.AuthApiLoginConfig;
import com.learning.kindajava.common.dto.CommonDto;
import com.learning.kindajava.common.dto.ResultDto;
import com.learning.kindajava.infrastructure.dto.AuthRequestParam;
import com.learning.kindajava.infrastructure.dto.AuthResponseParam;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class LoginClient {

    private final AuthApiLoginConfig authApiLoginConfig;

    private RestTemplate restTemplate = new RestTemplate();

    /**
     * LoginApi実行
     * 
     * @param authRequestParam 認証リクエスト
     * @return 認証結果
     */
    public AuthResponseParam execute(AuthRequestParam authRequestParam) {
        HttpEntity<MultiValueMap<String, String>> req = this.createRequest(authRequestParam);
        return doPost(req);
    }

    /**
     * リクエスト生成。
     * 
     * @param authRequestParam リクエスト
     * @return HttpEntity
     */
    private HttpEntity<MultiValueMap<String, String>> createRequest(
        AuthRequestParam authRequestParam) {
        return new HttpEntity<MultiValueMap<String, String>>(
            this.createMap(authRequestParam),
            this.createHeaders()
        );
    }

    /**
     * HTTPヘッダー生成。
     * 
     * @return HTTPヘッダー
     */
    private HttpHeaders createHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        return headers;
    }

    /**
     * Map生成。
     * 
     * @param authRequestParam リクエスト
     * @return Map
     */
    private MultiValueMap<String, String> createMap(AuthRequestParam authRequestParam) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("id", authRequestParam.getId());
        map.add("pass", authRequestParam.getPass());
        return map;
    }

    /**
     * AuthApiPost通信
     * 
     * @param req 認証APIリクエスト
     * @return 認証結果
     */
    private AuthResponseParam doPost(HttpEntity<MultiValueMap<String, String>> req) {
        try {
            return this.restTemplate.postForObject(
                authApiLoginConfig.getUri(),
                req,
                AuthResponseParam.class
            );
        }catch (HttpClientErrorException e) {
            return new AuthResponseParam(
                new CommonDto(new ResultDto(WARNING)),
                null,
                null,
                null
            );
        }
    }
}