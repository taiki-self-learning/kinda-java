package com.learning.kindajava.application.controller;

import static com.learning.kindajava.common.Constant.CookieItem.USER_ID;
import static com.learning.kindajava.common.util.Utility.getCookie;
import static com.learning.kindajava.common.util.Utility.date2String;
import static com.learning.kindajava.common.util.Utility.string2Date;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.learning.kindajava.application.payload.AttendancePayload;
import com.learning.kindajava.application.payload.AttendanceRequest;
import com.learning.kindajava.application.payload.AttendanceResponse;
import com.learning.kindajava.common.Constant.DateFormat;
import com.learning.kindajava.domain.model.AttendanceInfo;
import com.learning.kindajava.domain.model.AttendanceInfoRequest;
import com.learning.kindajava.domain.model.AttendanceInfoResponse;
import com.learning.kindajava.domain.service.AttendanceMngService;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/api/v1/attendance")
@RestController
public class AttendanceMngController {

    private final AttendanceMngService attendanceMngService;

    /**
     * 勤怠情報操作（GET）。
     * 
     * @param httpRequest    HttpServletRequest
     * @param operateDiv     操作区分
     * @param attendanceDate 勤怠日付
     * @return
     * @throws ParseException
     * @throws UnsupportedEncodingException
     */
    @GetMapping("/operate")
    public AttendanceResponse operate(HttpServletRequest httpRequest,
        @RequestParam(name = "operateDiv") String operateDiv,
        @RequestParam(name = "attendanceDate") String attendanceDateStr)
        throws ParseException, UnsupportedEncodingException {

        // 勤怠日付をDate型に変換
        Date attendanceDate = string2Date(attendanceDateStr, DateFormat.YYYYMMDD_HYPHEN);

        AttendanceInfoResponse response =
            this.attendanceMngService.operateAttendanceInfo(
                this.createAttendanceInfoRequest(httpRequest, operateDiv, attendanceDate));

        // 参照結果をPayloadに詰め替え返却
        return this.createAttendanceResponse(response);
    }

    /**
     * 勤怠情報操作（POST）。
     * 
     * @param request     勤怠リクエスト情報
     * @param httpRequest HttpServletRequest
     * @return
     * @throws ParseException
     * @throws UnsupportedEncodingException
     */
    @PostMapping("/operate")
    public AttendanceResponse operate(@RequestBody @Validated AttendanceRequest request,
        HttpServletRequest httpRequest) throws ParseException, UnsupportedEncodingException {

        AttendanceInfoResponse response = this.attendanceMngService
            .operateAttendanceInfo(this.createAttendanceInfoRequest(request, httpRequest));

        // 参照結果をPayloadに詰め替え返却
        return this.createAttendanceResponse(response);
    }

    /**
     * 勤怠情報モデルリクエスト生成。
     * 
     * @param HttpServletRequest HttpServletRequest
     * @param operateDiv         操作区分
     * @param attendanceDate     勤怠日付
     * @return 勤怠情報モデルリクエスト
     * @throws UnsupportedEncodingException
     */
    private AttendanceInfoRequest createAttendanceInfoRequest(
        HttpServletRequest httpRequest, String operateDiv, Date attendanceDate)
            throws UnsupportedEncodingException {
        return new AttendanceInfoRequest(
            operateDiv, this.createAttendanceInfoList(httpRequest, attendanceDate));
    }

    /**
     * 勤怠情報リスト生成。
     * 
     * @param HttpServletRequest HttpServletRequest
     * @param attendanceDate     勤怠日付
     * @return 勤怠情報モデルリクエスト
     * @throws UnsupportedEncodingException
     */
    private List<AttendanceInfo> createAttendanceInfoList(
        HttpServletRequest httpRequest, Date attendanceDate) throws UnsupportedEncodingException {
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
            attendanceInfoList.add(
                new AttendanceInfo(
                    getCookie(httpRequest, USER_ID),
                    attendanceDate,
                    null,
                    null,
                    null,
                    null,
                    0,
                    null
                )
            );
        return attendanceInfoList;
    }

    /**
     * 勤怠情報モデルリクエスト生成。
     * 
     * @param request     勤怠リクエスト
     * @param httpRequest HttpServletRequest
     * @return 勤怠情報モデルリクエスト
     * @throws ParseException
     * @throws UnsupportedEncodingException
     */
    private AttendanceInfoRequest createAttendanceInfoRequest(
        AttendanceRequest request, HttpServletRequest httpRequest)
            throws ParseException, UnsupportedEncodingException {
        return new AttendanceInfoRequest(
            request.getOperateDiv(),
            this.createAttendanceInfoList(request.getAttendancePayloadList(), httpRequest)
        );
    }

    /**
     * 勤怠情報リスト生成。
     * 
     * @param payloadList 勤怠ペイロードリスト
     * @param httpRequest HttpServletRequest
     * @return 勤怠情報モデルリクエスト
     * @throws ParseException
     * @throws UnsupportedEncodingException
     */
    private List<AttendanceInfo> createAttendanceInfoList(List<AttendancePayload> payloadList,
        HttpServletRequest httpRequest) throws ParseException, UnsupportedEncodingException {
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
        for (AttendancePayload payload : payloadList) {
            attendanceInfoList.add(
                new AttendanceInfo(
                    getCookie(httpRequest, USER_ID),
                    string2Date(payload.getAttendanceDate(), DateFormat.YYYYMMDD_HYPHEN),
                    payload.getAttendanceDiv(),
                    payload.getAttendanceDivDetail(),
                    string2Date(payload.getPunchInTime(), DateFormat.HHMMSS),
                    string2Date(payload.getPunchOutTime(), DateFormat.HHMMSS),
                    payload.getRestTime(),
                    payload.getAttendanceReason()
                )
            );
        }
        return attendanceInfoList;
    }

    /**
     * 勤怠参照レスポンス生成。
     * 
     * @param response 勤怠情報モデルレスポンス
     * @return 勤怠参照レスポンス
     * @throws ParseException
     */
    private AttendanceResponse createAttendanceResponse(AttendanceInfoResponse response)
        throws ParseException {
        return new AttendanceResponse(
            response.getCommonDto(),
            this.createAttendancePayload(response.getAttendanceInfoList())
        );
    }

    /**
     * 勤怠ペイロードリスト生成。
     * 
     * @param responseList 勤怠情報リスト
     * @return 勤怠参照レスポンス
     * @throws ParseException
     */
    private List<AttendancePayload> createAttendancePayload(List<AttendanceInfo> responseList)
        throws ParseException {
        List<AttendancePayload> payloadList = new ArrayList<>();
        for (AttendanceInfo response : responseList) {
            payloadList.add(
                new AttendancePayload(
                    response.getStaffId(),
                    date2String(response.getAttendanceDate(), DateFormat.YYYYMMDD_HYPHEN),
                    response.getAttendanceDiv(),
                    response.getAttendanceDivDetail(),
                    date2String(response.getPunchInTime(), DateFormat.HHMMSS),
                    date2String(response.getPunchOutTime(), DateFormat.HHMMSS),
                    response.getRestTime(),
                    response.getAttendanceReason()
                )
            );
        }
        return payloadList;
    }
}