package com.learning.kindajava.application.controller;

import static com.learning.kindajava.common.Constant.CookieItem.USER_ID;
import static com.learning.kindajava.common.Constant.OperateDiv.READ_MONTHLY;
import static com.learning.kindajava.common.util.Utility.date2String;
import static com.learning.kindajava.common.util.Utility.getCookie;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.learning.kindajava.application.payload.AttendancePayload;
import com.learning.kindajava.application.payload.AttendanceResponse;
import com.learning.kindajava.common.Constant.DateFormat;
import com.learning.kindajava.domain.model.AttendanceInfo;
import com.learning.kindajava.domain.model.AttendanceInfoRequest;
import com.learning.kindajava.domain.model.AttendanceInfoResponse;
import com.learning.kindajava.domain.service.AttendanceMngService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import lombok.RequiredArgsConstructor;

/**
 * スケジュール画面のコントローラ。
 */
@RequiredArgsConstructor
@Controller
public class ScheduleController {

    private final AttendanceMngService attendanceMngService;

    /**
     * スケジュール画面生成。
     * 
     * @param model モデル
     * @return スケジュール画面
     * @throws ParseException
     * @throws UnsupportedEncodingException
     */
    @GetMapping("/schedule")
    private String getSchedulePage(HttpServletRequest request, Model model)
            throws ParseException, UnsupportedEncodingException {
        AttendanceInfoResponse response = this.attendanceMngService
            .operateAttendanceInfo(this.createAttendanceInfoRequest(request));
        model.addAttribute("attendanceInfo", this.createAttendanceResponse(response));
        return "pages/schedule";
    }

    /**
     * 勤怠情報リクエスト生成。
     * 
     * @param request HttpServletRequest
     * @return 勤怠情報リクエスト
     * @throws UnsupportedEncodingException
     */
    private AttendanceInfoRequest createAttendanceInfoRequest(HttpServletRequest request)
        throws UnsupportedEncodingException {
        return new AttendanceInfoRequest(READ_MONTHLY, this.createAttendanceInfoList(request));
    }

    /**
     * 勤怠情報リスト生成。
     * 
     * @param request HttpServletRequest
     * @return 勤怠情報リスト
     * @throws UnsupportedEncodingException
     */
    private List<AttendanceInfo> createAttendanceInfoList(HttpServletRequest request)
            throws UnsupportedEncodingException {
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
        attendanceInfoList.add(
            new AttendanceInfo(
                getCookie(request, USER_ID),
                new Date(),
                null,
                null,
                null,
                null,
                0,
                null
            )
        );
        return attendanceInfoList;
    }

    /**
     * 勤怠参照レスポンス生成。
     * 
     * @param response 勤怠情報モデルレスポンス
     * @return 勤怠参照レスポンス
     * @throws ParseException
     */
    private AttendanceResponse createAttendanceResponse(AttendanceInfoResponse response)
        throws ParseException {
        return new AttendanceResponse(
            response.getCommonDto(),
            this.createAttendancePayload(response.getAttendanceInfoList())
        );
    }

    /**
     * 勤怠ペイロードリスト生成。
     * 
     * @param responseList 勤怠情報リスト
     * @return 勤怠参照レスポンス
     * @throws ParseException
     */
    private List<AttendancePayload> createAttendancePayload(List<AttendanceInfo> responseList)
        throws ParseException {
        List<AttendancePayload> payloadList = new ArrayList<>();
        for (AttendanceInfo response : responseList) {
            payloadList.add(
                new AttendancePayload(
                    response.getStaffId(),
                    date2String(response.getAttendanceDate(), DateFormat.YYYYMMDD_HYPHEN),
                    response.getAttendanceDiv(),
                    response.getAttendanceDivDetail(),
                    date2String(response.getPunchInTime(), DateFormat.HHMMSS),
                    date2String(response.getPunchOutTime(), DateFormat.HHMMSS),
                    response.getRestTime(),
                    response.getAttendanceReason()
                )
            );
        }
        return payloadList;
    }
}
