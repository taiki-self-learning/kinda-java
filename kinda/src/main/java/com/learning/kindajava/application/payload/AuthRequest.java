package com.learning.kindajava.application.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 認証情報。
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AuthRequest {

    /** id */
    private String id;

    /** パスワード */
    private String password;
}
