package com.learning.kindajava.application.controller;

import static com.learning.kindajava.common.Constant.CookieItem.APP_TOKEN;
import static com.learning.kindajava.common.Constant.ResultCd.SUCCESS;
import static com.learning.kindajava.common.Constant.ResultCd.WARNING;
import static com.learning.kindajava.common.Constant.CookieItem.ROLE;
import static com.learning.kindajava.common.Constant.CookieItem.USER_ID;
import static com.learning.kindajava.common.Constant.CookieItem.USER_NAME;
import static com.learning.kindajava.common.util.Utility.createJwtToken;
import static com.learning.kindajava.common.util.Utility.removeAllCookie;
import static com.learning.kindajava.common.util.Utility.setCookie;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.learning.kindajava.application.payload.AuthRequest;
import com.learning.kindajava.application.payload.AuthResponse;
import com.learning.kindajava.domain.model.AuthInfoRequest;
import com.learning.kindajava.domain.model.AuthInfoResponse;
import com.learning.kindajava.domain.service.AuthService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.RequiredArgsConstructor;

/**
 * ログイン認証のコントローラ。
 */
@RequiredArgsConstructor
@Controller
public class AuthController {

    private final AuthService service;

    /**
     * ログイン認証。
     * 
     * @param authRequest 認証リクエスト
     * @param request     Httpリクエスト
     * @param response    Httpレスポンス
     * @param model       モデル
     * @return リダイレクト処理
     * @throws UnsupportedEncodingException
     */
    @GetMapping("/dologin")
    private String doLogin(@ModelAttribute AuthRequest authRequest, HttpServletRequest request,
        HttpServletResponse response, Model model) throws UnsupportedEncodingException {

        // service呼出
        AuthInfoResponse authInfoResponse = this.service.doLogin(this.createAuthInfoReqest(authRequest));

        // 結果コード判定
        String resultCd = authInfoResponse.getCommonDto().getResult().getResultcd();
        if (SUCCESS.equals(resultCd)) {
            this.auth2Cookie(response, this.createAuthResponse(authInfoResponse));
            return "redirect:/home";
        } else if (WARNING.equals(resultCd)) {
            removeAllCookie(request, response);
            return "redirect:/login?error";
        } else {
            // TODO:error handle
            removeAllCookie(request, response);
            return "redirect:/error";
        }
    }

    /**
     * ログアウト処理。
     * 
     * @param authRequest 認証リクエスト
     * @param request     Httpリクエスト
     * @param response    Httpレスポンス
     * @param model       モデル
     * @return ログイン画面
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    private String doLogout(HttpServletRequest request, HttpServletResponse response, Model model) {
        removeAllCookie(request, response);
        return "redirect:/login";
    }

    /**
     * AuthInfoRequest生成。
     * 
     * @param authRequest 認証リクエスト情報
     * @return AuthInfoRequest
     */
    private AuthInfoRequest createAuthInfoReqest(AuthRequest authRequest) {
        return new AuthInfoRequest(authRequest.getId(), authRequest.getPassword());
    }

    /**
     * AuthResponse生成。
     * 
     * @param authInfoResponse 認証レスポンスモデル
     * @return AuthResponse
     */
    private AuthResponse createAuthResponse(AuthInfoResponse authInfoResponse) {
        return new AuthResponse(authInfoResponse.getUserId(), authInfoResponse.getUserName(),
                authInfoResponse.getRole());
    }

    /**
     * 認証情報クッキー設定
     * 
     * @param response HttpServletResponse
     * @param authInfo 認証情報
     * @throws UnsupportedEncodingException
     */
    private void auth2Cookie(HttpServletResponse hResponse, AuthResponse authResponse)
            throws UnsupportedEncodingException {

        // ユーザID
        setCookie(hResponse, USER_ID, authResponse.getUserId());

        // ユーザ名
        String userName = authResponse.getUserName();
        setCookie(hResponse, USER_NAME, userName);

        // 権限
        String role = authResponse.getRole();
        setCookie(hResponse, ROLE, role);
        
        // token
        setCookie(hResponse, APP_TOKEN, createJwtToken(userName, role));
    }
}
