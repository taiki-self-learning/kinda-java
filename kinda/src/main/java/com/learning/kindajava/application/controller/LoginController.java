package com.learning.kindajava.application.controller;

import com.learning.kindajava.application.payload.AuthRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * ログイン認証のコントローラ。
 */
@Controller
public class LoginController {

    /**
     * ログイン認証。
     * @param model モデル
     * @return ログイン画面
     */
    @GetMapping("/login")
    private String getLoginPage(Model model) {
        model.addAttribute("authRequest", new AuthRequest());
        return "login";
    }
}
