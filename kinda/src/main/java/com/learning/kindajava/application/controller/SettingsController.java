package com.learning.kindajava.application.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 設定画面のコントローラ。
 */
@Controller
public class SettingsController {

    /**
     * 設定画面生成。
     * @param model モデル
     * @return 設定画面
     */
    @GetMapping("/settings")
    private String getDetailPage(Model model) {
        return "pages/settings";
    }
}
