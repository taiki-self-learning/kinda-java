package com.learning.kindajava.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class AuthInfoRequest {

    /** id */
    private String id;

    /** パスワード */
    private String password;
}