package com.learning.kindajava.domain.model;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class AttendanceInfoRequest {

    // 操作区分
    private String operateDiv;

    // 勤怠情報リスト
    private List<AttendanceInfo> attendanceInfoList;

    /**
     * 社員ID取得。
     * 
     * @param index インデックス
     * @return 社員ID
     */
    public String getStaffId(int index) {
        return this.attendanceInfoList.get(index).getStaffId();
    }

    /**
     * 日付取得。
     * 
     * @param index インデックス
     * @return 日付
     */
    public Date getAttendanceDate(int index) {
        return this.attendanceInfoList.get(index).getAttendanceDate();
    }

    /**
     * 勤怠区分取得。
     * 
     * @param index インデックス
     * @return 勤怠区分
     */
    public String getAttendanceDiv(int index) {
        return this.attendanceInfoList.get(index).getAttendanceDiv();
    }

    /**
     * 勤怠区分詳細取得。
     * 
     * @param index インデックス
     * @return 勤怠区分詳細
     */
    public String getAttendanceDivDetail(int index) {
        return this.attendanceInfoList.get(index).getAttendanceDivDetail();
    }

    /**
     * 出勤時刻取得。
     * 
     * @param index インデックス
     * @return 出勤時刻
     */
    public Date getPunchInTime(int index) {
        return this.attendanceInfoList.get(index).getPunchInTime();
    }

    /**
     * 退勤時刻取得。
     * 
     * @param index インデックス
     * @return 退勤時刻
     */
    public Date getPunchOutTime(int index) {
        return this.attendanceInfoList.get(index).getPunchOutTime();
    }

    /**
     * 休憩時間取得。
     * 
     * @param index インデックス
     * @return 休憩時間
     */
    public int getRestTime(int index) {
        return this.attendanceInfoList.get(index).getRestTime();
    }

    /**
     * 勤怠理由取得。
     * 
     * @param index インデックス
     * @return 勤怠理由
     */
    public String getAttendanceReason(int index) {
        return this.attendanceInfoList.get(index).getAttendanceReason();
    }
}