package com.learning.kindajava.domain.model;

import java.util.List;

import com.learning.kindajava.common.dto.CommonDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AttendanceInfoResponse {

    /** 共通DTO */
    private CommonDto commonDto;
    
    // 勤怠情報リスト
    private List<AttendanceInfo> attendanceInfoList;
}