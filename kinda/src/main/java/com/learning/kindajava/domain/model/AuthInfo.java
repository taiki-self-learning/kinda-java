package com.learning.kindajava.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AuthInfo {

    /** 認証情報リクエスト */
    private AuthInfoRequest request;

    /** 認証情報レスポンス */
    private AuthInfoResponse response;
}