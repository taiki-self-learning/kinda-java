package com.learning.kindajava.domain.repository;

import com.learning.kindajava.domain.model.AttendanceInfoRequest;
import com.learning.kindajava.domain.model.AttendanceInfoResponse;

public interface AttendanceMngRepository {

    /**
     * 勤怠管理テーブル参照処理。
     * 
     * @param request 勤怠情報モデルリクエスト
     * @return 勤怠情報モデルレスポンス
     */
    public AttendanceInfoResponse read(AttendanceInfoRequest request);

    
    /**
     * 勤怠管理テーブル参照（指定月）処理。
     * 
     * @param request 勤怠情報モデルリクエスト
     * @return 勤怠情報モデルレスポンス
     */
    public AttendanceInfoResponse readMonthly(AttendanceInfoRequest request);

    /**
     * 勤怠管理テーブル登録処理。
     * 
     * @param request 勤怠情報モデルリクエスト
     * @return 勤怠情報モデルレスポンス
     */
    public AttendanceInfoResponse create(AttendanceInfoRequest request);

    /**
     * 勤怠管理テーブル更新処理。
     * 
     * @param request 勤怠情報モデルリクエスト
     * @return 勤怠情報モデルレスポンス
     */
    public AttendanceInfoResponse update(AttendanceInfoRequest request);
}