package com.learning.kindajava.domain.service;

import static com.learning.kindajava.common.Constant.OperateDiv.CREATE;
import static com.learning.kindajava.common.Constant.OperateDiv.DELETE;
import static com.learning.kindajava.common.Constant.OperateDiv.READ_DAILY;
import static com.learning.kindajava.common.Constant.OperateDiv.READ_MONTHLY;
import static com.learning.kindajava.common.Constant.OperateDiv.UPDATE;

import com.learning.kindajava.domain.model.AttendanceInfoRequest;
import com.learning.kindajava.domain.model.AttendanceInfoResponse;
import com.learning.kindajava.domain.repository.AttendanceMngRepository;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class AttendanceMngService {

    private final AttendanceMngRepository attendanceMngRepository;

    /**
     * 勤怠情報操作処理。
     * 
     * @param request 勤怠情報リクエストモデル
     * @return 勤怠情報レスポンスモデル
     */
    public AttendanceInfoResponse operateAttendanceInfo(AttendanceInfoRequest request) {
        AttendanceInfoResponse response = new AttendanceInfoResponse();

        // 操作区分判定
        if (CREATE.equals(request.getOperateDiv())) {
            response = this.attendanceMngRepository.create(request);
        }else if (READ_DAILY.equals(request.getOperateDiv())) {
            response = this.attendanceMngRepository.read(request);
        }else if (READ_MONTHLY.equals(request.getOperateDiv())) {
            response = this.attendanceMngRepository.readMonthly(request);
        }else if (UPDATE.equals(request.getOperateDiv())) {
            response = this.attendanceMngRepository.update(request);
        }else if (DELETE.equals(request.getOperateDiv())) {
            // TODO:delete処理記載
        }
        return response;
    }
}