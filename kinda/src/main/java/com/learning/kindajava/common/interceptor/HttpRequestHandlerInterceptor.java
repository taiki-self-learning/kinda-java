package com.learning.kindajava.common.interceptor;

import static com.learning.kindajava.common.Constant.CookieItem.APP_TOKEN;
import static com.learning.kindajava.common.Constant.CookieItem.ROLE;
import static com.learning.kindajava.common.Constant.CookieItem.SECRET_KEY;
import static com.learning.kindajava.common.Constant.CookieItem.USER_ID;
import static com.learning.kindajava.common.Constant.CookieItem.USER_NAME;
import static com.learning.kindajava.common.util.Utility.createJwtToken;
import static com.learning.kindajava.common.util.Utility.getCookie;
import static com.learning.kindajava.common.util.Utility.isAdmin;
import static com.learning.kindajava.common.util.Utility.setCookie;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HttpRequestHandlerInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
        Object handler) throws Exception {
        log.info("preHandle : " + request.getRequestURI());

        // 認証済み判定
        String token = getCookie(request, APP_TOKEN);
        if (StringUtils.isNotEmpty(token)) {

            // 認証済の為、トークンを更新
            try {
                setCookie(response, APP_TOKEN, this.refreshToken(token));
                return true;
            }catch (JWTVerificationException e) {

                // Token期限切れのため、ログアウト
                response.sendRedirect(request.getContextPath() + "/logout");
                return false;
            }
        } else {

            // 未認証の為、認証画面へ遷移
            response.sendRedirect(request.getContextPath() + "/login");
            return false;
        }
    }
    
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response,
        Object handler, ModelAndView modelAndView) throws Exception {
        modelAndView.addObject("userId", getCookie(request, USER_ID));
        modelAndView.addObject("userName", getCookie(request, USER_NAME));
        modelAndView.addObject("isAdmin", isAdmin(getCookie(request, ROLE)));
    }
    
    /**
     * jwt token更新。
     * 
     * @param token token
     * @return 更新されたtoken
     * @throws UnsupportedEncodingException
     * @throws IllegalArgumentException
     */
    private String refreshToken(String token) throws Exception {
        Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY);
        JWTVerifier verifier = JWT.require(algorithm).build();
        
        // tokenチェック
        DecodedJWT jwt = verifier.verify(token);
        
        String userName = jwt.getClaim(USER_NAME).asString();
        String role = jwt.getClaim(ROLE).asString();
        
        // トークン更新
        token = createJwtToken(userName, role);
        
        return token;
    }
}