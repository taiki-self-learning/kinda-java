package com.learning.kindajava.common.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "auth-api.login")
public class AuthApiLoginConfig {

    private String uri;
}