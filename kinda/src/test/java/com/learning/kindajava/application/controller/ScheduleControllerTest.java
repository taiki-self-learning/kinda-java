package com.learning.kindajava.application.controller;

import static com.learning.kindajava.common.Constant.AttendanceCd.PUNCH_IN;
import static com.learning.kindajava.common.Constant.AttendanceCd.PUNCH_OUT;
import static com.learning.kindajava.common.Constant.CookieItem.ROLE;
import static com.learning.kindajava.common.Constant.CookieItem.USER_NAME;
import static com.learning.kindajava.common.Constant.CookieItem.USER_ID;
import static com.learning.kindajava.common.Constant.ResultCd.SUCCESS;
import static com.learning.kindajava.common.util.Utility.date2String;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;

import com.learning.kindajava.application.payload.AttendancePayload;
import com.learning.kindajava.application.payload.AttendanceResponse;
import com.learning.kindajava.common.Constant.DateFormat;
import com.learning.kindajava.common.dto.CommonDto;
import com.learning.kindajava.common.dto.ResultDto;
import com.learning.kindajava.domain.model.AttendanceInfo;
import com.learning.kindajava.domain.model.AttendanceInfoRequest;
import com.learning.kindajava.domain.model.AttendanceInfoResponse;
import com.learning.kindajava.domain.service.AttendanceMngService;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ScheduleController.class)
public class ScheduleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AttendanceMngService service;

    @Disabled
    @Test
    @DisplayName("/scheduleリクエスト時、model情報を設定しスケジュール画面を生成すること")
    void getSchedulePageTest() throws Exception {

        // setup
        Cookie cookieId = new Cookie(USER_ID, "000000");
        Cookie cookieUser = new Cookie(USER_NAME, "testUser");
        Cookie cookieRole = new Cookie(ROLE, "ROLE_USER");
        Date date = new Date();
        doReturn(this.createAttendanceInfoResponse(date))
            .when(this.service).operateAttendanceInfo(any(AttendanceInfoRequest.class));

        // execute
        this.mockMvc.perform(get("/schedule").cookie(cookieId, cookieUser, cookieRole))
            .andExpect(status().isOk())
            .andExpect(view().name("schedule"))
            .andExpect(model().attribute("attendanceInfo", this.createAttendanceResponse(date)));
    }

    /**
     * 勤怠情報レスポンス生成。
     * 
     * @param date 日付
     * @return 勤怠情報レスポンス
     */
    private AttendanceInfoResponse createAttendanceInfoResponse(Date date) {
        return new AttendanceInfoResponse(
            new CommonDto(new ResultDto(SUCCESS)),
            this.createAttendanceInfo(date)
        );
    }

    /**
     * 勤怠情報生成。
     * 
     * @param date 日付
     * @return 勤怠情報
     */
    private List<AttendanceInfo> createAttendanceInfo(Date date) {
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
        attendanceInfoList.add(
            new AttendanceInfo(
                "000000",
                date,
                PUNCH_IN,
                null,
                date,
                null,
                0,
                null
            )
        );
        attendanceInfoList.add(
            new AttendanceInfo(
                "000000",
                date,
                PUNCH_OUT,
                null,
                date,
                date,
                60,
                null
            )
        );
        return attendanceInfoList;
    }

    /**
     * 勤怠レスポンス生成。
     * 
     * @param date 日付
     * @return 勤怠レスポンス
     * @throws ParseException
     */
    private AttendanceResponse createAttendanceResponse(Date date) throws ParseException {
        return new AttendanceResponse(
            new CommonDto(new ResultDto(SUCCESS)),
            this.createAttendancePayloadList(date)
        );
    }

    /**
     * 勤怠ペイロードリスト生成。
     * 
     * @param date 日付
     * @return 勤怠ペイロードリスト
     * @throws ParseException
     */
    private List<AttendancePayload> createAttendancePayloadList(Date date) throws ParseException {
        List<AttendancePayload> attendancePayloadList = new ArrayList<>();
        attendancePayloadList.add(
            new AttendancePayload(
                "000000",
                date2String(date, DateFormat.YYYYMMDD_HYPHEN),
                PUNCH_IN,
                null,
                date2String(date, DateFormat.HHMMSS),
                null,
                0,
                null
            )
        );
        attendancePayloadList.add(
            new AttendancePayload(
                "000000",
                date2String(date, DateFormat.YYYYMMDD_HYPHEN),
                PUNCH_OUT,
                null,
                date2String(date, DateFormat.HHMMSS),
                date2String(date, DateFormat.HHMMSS),
                60,
                null
            )
        );
        return attendancePayloadList;
    }
}