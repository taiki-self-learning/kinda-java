package com.learning.kindajava.application.controller;

import static com.learning.kindajava.common.Constant.CookieItem.ROLE;
import static com.learning.kindajava.common.Constant.CookieItem.USER_NAME;
import static com.learning.kindajava.common.Constant.CookieItem.USER_ID;
import static com.learning.kindajava.common.Constant.ResultCd.SUCCESS;
import static com.learning.kindajava.common.util.Utility.date2String;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;

import com.learning.kindajava.application.payload.AttendancePayload;
import com.learning.kindajava.application.payload.AttendanceResponse;
import com.learning.kindajava.common.Constant.AttendanceCd;
import com.learning.kindajava.common.Constant.DateFormat;
import com.learning.kindajava.common.dto.CommonDto;
import com.learning.kindajava.common.dto.ResultDto;
import com.learning.kindajava.domain.model.AttendanceInfo;
import com.learning.kindajava.domain.model.AttendanceInfoRequest;
import com.learning.kindajava.domain.model.AttendanceInfoResponse;
import com.learning.kindajava.domain.service.AttendanceMngService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@SpringBootTest
public class HomeControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private HomeController controller;

    @MockBean
    private AttendanceMngService service;

    @BeforeEach
    void setUp() {
        this.mockMvc =
            MockMvcBuilders.standaloneSetup(controller)
                .setViewResolvers(new InternalResourceViewResolver("/templates/", ".html"))
                    .build();
    }

    @Test
    @DisplayName("/homeリクエスト時、modelへ勤怠情報を設定しホーム画面を生成すること")
    void getHomePageUnattendedTest() throws Exception {

        // setup
        Date date = new Date();
        Cookie cookieId = new Cookie(USER_ID, "000000");
        Cookie cookieUser = new Cookie(USER_NAME, "testUser");
        Cookie cookieRole = new Cookie(ROLE, "ROLE_USER");
        doReturn(this.createAttendanceInfoResponse(date)).when(this.service)
            .operateAttendanceInfo(any(AttendanceInfoRequest.class));

        // execute & assert
        this.mockMvc.perform(get("/home").cookie(cookieId, cookieUser, cookieRole))
            .andExpect(status().isOk())
            .andExpect(view().name("pages/home"))
            .andExpect(model().attribute("response", this.createAttendanceResponse(date)));
    }

    /**
     * 勤怠情報レスポンス生成。
     * 
     * @param date 日付
     * @return 勤怠情報レスポンス
     */
    private AttendanceInfoResponse createAttendanceInfoResponse(Date date) {
        return new AttendanceInfoResponse(
            new CommonDto(new ResultDto(SUCCESS)), this.createAttendanceInfoList(date)
        );
    }

    /**
     * 勤怠情報リスト生成。
     * 
     * @param date 日付
     * @return 勤怠情報リスト
     */
    private List<AttendanceInfo> createAttendanceInfoList(Date date) {
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
        attendanceInfoList.add(
            new AttendanceInfo("000000", date, AttendanceCd.PUNCH_IN, null, date, null, 0, null)
        );
        return attendanceInfoList;
    }

    /**
     * 勤怠参照レスポンス生成。
     * 
     * @param date 日付
     * @return 勤怠参照レスポンス
     * @throws ParseException
     */
    private AttendanceResponse createAttendanceResponse(Date date) throws ParseException {
        return new AttendanceResponse(
            new CommonDto(new ResultDto(SUCCESS)),
            this.createAttendancePayload(date)
        );
    }

    /**
     * 勤怠ペイロードリスト生成。
     * 
     * @param date 日付
     * @return 勤怠参照レスポンス
     * @throws ParseException
     */
    private List<AttendancePayload> createAttendancePayload(Date date) throws ParseException {
        List<AttendancePayload> payloadList = new ArrayList<>();
        payloadList.add(
            new AttendancePayload(
                "000000",
                date2String(date, DateFormat.YYYYMMDD_HYPHEN),
                AttendanceCd.PUNCH_IN,
                null,
                date2String(date, DateFormat.HHMMSS),
                null,
                0,
                null
            )
        );
        return payloadList;
    }
}