package com.learning.kindajava.application.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@WebMvcTest(DetailController.class)
public class DetailControllerTest {

    @Autowired
    private MockMvc mockMvc; 

    @Disabled
    @Test
    @DisplayName("/detailリクエスト時、詳細画面を生成すること")
    void getDetailPageTest() throws Exception {

        // execute & assert
        this.mockMvc.perform(get("/detail"))
            .andExpect(status().isOk()).andExpect(view().name("detail"));
    }
}