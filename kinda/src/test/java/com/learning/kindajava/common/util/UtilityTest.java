package com.learning.kindajava.common.util;

import static com.learning.kindajava.common.Constant.CookieItem.ROLE;
import static com.learning.kindajava.common.Constant.CookieItem.SECRET_KEY;
import static com.learning.kindajava.common.Constant.CookieItem.USER_NAME;
import static com.learning.kindajava.common.util.Utility.createJwtToken;
import static com.learning.kindajava.common.util.Utility.date2String;
import static com.learning.kindajava.common.util.Utility.getCookie;
import static com.learning.kindajava.common.util.Utility.getEndDay;
import static com.learning.kindajava.common.util.Utility.getFirstDay;
import static com.learning.kindajava.common.util.Utility.isAdmin;
import static com.learning.kindajava.common.util.Utility.removeAllCookie;
import static com.learning.kindajava.common.util.Utility.setCookie;
import static com.learning.kindajava.common.util.Utility.string2Date;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.Cookie;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.learning.kindajava.common.Constant.DateFormat;
import com.learning.kindajava.common.Constant.Role;

import org.apache.commons.lang.time.DateUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

public class UtilityTest {

    @Test
    @DisplayName("String型の日付を指定フォーマットでDate型に変換できること")
    void string2DateTeat() throws ParseException {

        // setup
        String stubDate = "20000101";
        Calendar cal = Calendar.getInstance();
        cal.set(2000, 0, 1);
        Date expected = cal.getTime();

        // execute
        Date actual = string2Date(stubDate, DateFormat.YYYYMMDD);

        // assert
        assertTrue(DateUtils.isSameDay(expected, actual));
    }

    @Test
    @DisplayName("Date型の日付を指定フォーマットでString型に変換できること")
    void date2StringTeat() throws ParseException {

        // setup
        Calendar cal = Calendar.getInstance();
        cal.set(2000, 0, 1);
        Date stubDate = cal.getTime();
        String expected = "20000101";

        // execute
        String actual = date2String(stubDate, DateFormat.YYYYMMDD);

        // assert
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("対象月の月初日を取得できること")
    void getFirstDayTeat() {

        // setup
        Calendar cal1 = Calendar.getInstance();
        cal1.set(2000, 0, 15);
        Date stubDate = cal1.getTime();
        Calendar cal2 = Calendar.getInstance();
        cal2.set(2000, 0, 1);
        Date expected = cal2.getTime();

        // execute
        Date actual = getFirstDay(stubDate);

        // assert
        assertTrue(DateUtils.isSameDay(expected, actual));
    }

    @Test
    @DisplayName("対象月の月末日を取得できること")
    void getEndDayTeat() {

        // setup
        Calendar cal1 = Calendar.getInstance();
        cal1.set(2000, 0, 15);
        Date stubDate = cal1.getTime();
        Calendar cal2 = Calendar.getInstance();
        cal2.set(2000, 0, 31);
        Date expected = cal2.getTime();

        // execute
        Date actual = getEndDay(stubDate);

        // assert
        assertTrue(DateUtils.isSameDay(expected, actual));
    }

    @Test
    @DisplayName("指定したcookieの値を取得できること")
    void getCookieTest() throws UnsupportedEncodingException {

        // setup
        MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        httpServletRequest.setCookies(new Cookie("test_cookie", "test"));

        // execute
        String actual = getCookie(httpServletRequest, "test_cookie");

        // assert
        assertEquals(actual, "test");
    }

    @Test
    @DisplayName("cookieの値を取得する際Cookieが存在しない場合、Nullを返却すること")
    void getCookieNullTest() throws UnsupportedEncodingException {

        // setup
        MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();

        // execute
        String actual = getCookie(httpServletRequest, "test_cookie");

        // assert
        assertEquals(actual, null);
    }

    @Test
    @DisplayName("指定したcookieの値を設定できること")
    void setCookieTest() throws UnsupportedEncodingException {

        // setup
        MockHttpServletResponse httpServletResponse = new MockHttpServletResponse();

        // execute
        setCookie(httpServletResponse, "test_cookie", "test");

        // assert
        assertEquals(httpServletResponse.getCookie("test_cookie").getValue(), "test");
    }

    @Test
    @DisplayName("cookieを全削除できること")
    void removeAllCookieTest() {

        // setup
        MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        MockHttpServletResponse httpServletResponse = new MockHttpServletResponse();
        httpServletRequest.setCookies(new Cookie("test_cookie1", "test1"), new Cookie("test_cookie2", "test2"));

        // execute
        removeAllCookie(httpServletRequest, httpServletResponse);

        // assert
        assertEquals(httpServletResponse.getCookie("test_cookie1").getMaxAge(), 0);
        assertEquals(httpServletResponse.getCookie("test_cookie2").getMaxAge(), 0);
    }

    @Test
    @DisplayName("権限が管理者であればTrueを返却すること")
    void isAdminTest() {

        // execute
        boolean actual = isAdmin(Role.ADMIN);

        // assert
        assertTrue(actual);
    }

    @Test
    @DisplayName("権限が管理者以外であればFalseを返却すること")
    void isNotAdminTest() {

        // execute
        boolean actual = isAdmin(Role.USER);

        // assert
        assertFalse(actual);
    }


    @Test
    @DisplayName("jwt tokenを作成できること")
    void createJwtTokenTest() throws Exception {

        // setup
        Date expectedDate = new Date();
        expectedDate.setTime(expectedDate.getTime() + 1800000l);
        expectedDate = DateUtils.truncate(expectedDate, Calendar.SECOND);
        Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY);
        JWTVerifier verifier = JWT.require(algorithm).build();

        // execute
        String actual = createJwtToken("testUser", "ROLE_USER");

        // assert
        assertEquals(verifier.verify(actual).getClaim(USER_NAME).asString(), "testUser");
        assertEquals(verifier.verify(actual).getClaim(ROLE).asString(), "ROLE_USER");
        assertEquals(verifier.verify(actual).getExpiresAt(), expectedDate);
    }
}