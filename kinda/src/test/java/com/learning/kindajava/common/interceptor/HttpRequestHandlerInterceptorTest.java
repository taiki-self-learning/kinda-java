package com.learning.kindajava.common.interceptor;

import static com.learning.kindajava.common.Constant.CookieItem.APP_TOKEN;
import static com.learning.kindajava.common.Constant.CookieItem.ROLE;
import static com.learning.kindajava.common.Constant.CookieItem.SECRET_KEY;
import static com.learning.kindajava.common.Constant.CookieItem.USER_ID;
import static com.learning.kindajava.common.Constant.CookieItem.USER_NAME;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.Cookie;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.learning.kindajava.common.Constant.Role;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

@SpringBootTest
public class HttpRequestHandlerInterceptorTest {

    @Autowired
    private HttpRequestHandlerInterceptor target;

    @Test
    @DisplayName("認証済みであればtokenを更新する")
    void preHandleLoginedTest() throws Exception {

        // setup
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        request.setCookies(
            new Cookie(APP_TOKEN, this.createJwtTokenStub("testUser", "ROLE_USER")));
        Date expectedDate = new Date();
        expectedDate.setTime(expectedDate.getTime() + 1800000l);
        expectedDate = DateUtils.truncate(expectedDate, Calendar.MINUTE);
        Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY);
        JWTVerifier verifier = JWT.require(algorithm).build();

        // execute
        boolean actual = this.target.preHandle(request, response, new Object());

        // assert
        Date expiresAt = verifier.verify(this.getCookie(response, APP_TOKEN)).getExpiresAt();
        assertEquals(DateUtils.truncate(expiresAt, Calendar.MINUTE), expectedDate);
        assertTrue(actual);
    }

    @Test
    @DisplayName("未認証であれば/loginへリダイレクトを行うこと")
    void preHandleNonLoginTest() throws Exception {

        // setup
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        request.setCookies(new Cookie(APP_TOKEN, StringUtils.EMPTY));

        // execute
        boolean actual = this.target.preHandle(request, response, new Object());

        // assert
        assertEquals(response.getRedirectedUrl(), "/login");
        assertFalse(actual);
    }

    @Test
    @DisplayName("Token期限切れであれば/logoutへリダイレクトを行うこと")
    void preHandleExpiredTest() throws Exception {

        // setup
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        request.setCookies(
            new Cookie(APP_TOKEN, this.createJwtTokenExpiredStub("testUser", "ROLE_USER")));

        // execute
        boolean actual = this.target.preHandle(request, response, new Object());

        // assert
        assertEquals(response.getRedirectedUrl(), "/logout");
        assertFalse(actual);
    }

    @Test
    @DisplayName("Token期限切れであれば/logoutへリダイレクトを行うこと")
    void postHandleTest() throws Exception {

        // setup
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        ModelAndView mav = new ModelAndView();
        Cookie userId = new Cookie(USER_ID, "000000");
        Cookie userName = new Cookie(USER_NAME, "testUser");
        Cookie isAdmin = new Cookie(ROLE, Role.USER);
        request.setCookies(userId, userName, isAdmin);

        // execute
        this.target.postHandle(request, response, new Object(), mav);

        // assert
        assertAll(
            () -> { assertEquals(mav.getModel().get("userId"), "000000"); },
            () -> { assertEquals(mav.getModel().get("userName"), "testUser"); },
            () -> { assertEquals(mav.getModel().get("isAdmin"), false); }
        );
    }

    /**
     * jwt tokenスタブ生成。
     * 
     * @param userName ログインユーザ名
     * @param role ログインユーザロール
     * @return token
     */
    private String createJwtTokenStub(String userName, String role) {
        String token = null;
        try {
            Date issuedTime = new Date();
            Date expireTime = new Date();
            expireTime.setTime(expireTime.getTime() + 600000l);

            Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY);
            token = JWT.create()
                .withIssuer("auth0")
                .withIssuedAt(issuedTime)
                .withExpiresAt(expireTime)
                .withClaim(USER_NAME, userName)
                .withClaim(ROLE, role)
                .sign(algorithm);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return token;
    }

    /**
     * jwt token(期限切れ)スタブ生成。
     * 
     * @param userName ログインユーザ名
     * @param role ログインユーザロール
     * @return token
     */
    private String createJwtTokenExpiredStub(String userName, String role) {
        String token = null;
        try {
            Date issuedTime = new Date();
            Date expireTime = new Date();
            issuedTime.setTime(issuedTime.getTime() - 600000l);
            expireTime.setTime(expireTime.getTime() - 600000l);

            Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY);
            token = JWT.create()
                .withIssuer("auth0")
                .withIssuedAt(issuedTime)
                .withExpiresAt(expireTime)
                .withClaim(USER_NAME, userName)
                .withClaim(ROLE, role)
                .sign(algorithm);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return token;
    }

    /**
     * Cookie取得。
     * 
     * @param request HttpServletRequest
     * @param cookieName cookie名
     * @return cookie
     */
    private String getCookie(MockHttpServletResponse response, String cookieName) {
        String cookieValue = null;
        Cookie[] cookies = response.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookieName.equals(cookie.getName())) {
                    cookieValue = cookie.getValue();
                }
            }
        }
        return cookieValue;
    }
}