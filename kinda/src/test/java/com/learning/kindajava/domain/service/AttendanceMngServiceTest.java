package com.learning.kindajava.domain.service;

import static com.learning.kindajava.common.Constant.AttendanceCd.PUNCH_IN;
import static com.learning.kindajava.common.Constant.AttendanceCd.PUNCH_OUT;
import static com.learning.kindajava.common.Constant.OperateDiv.CREATE;
import static com.learning.kindajava.common.Constant.OperateDiv.READ_DAILY;
import static com.learning.kindajava.common.Constant.OperateDiv.READ_MONTHLY;
import static com.learning.kindajava.common.Constant.OperateDiv.UPDATE;
import static com.learning.kindajava.common.Constant.ResultCd.SUCCESS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.learning.kindajava.common.dto.CommonDto;
import com.learning.kindajava.common.dto.ResultDto;
import com.learning.kindajava.domain.model.AttendanceInfo;
import com.learning.kindajava.domain.model.AttendanceInfoRequest;
import com.learning.kindajava.domain.model.AttendanceInfoResponse;
import com.learning.kindajava.domain.repository.AttendanceMngRepository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class AttendanceMngServiceTest {

    @InjectMocks
    AttendanceMngService target;

    @Mock
    AttendanceMngRepository repository;

    @Test
    @DisplayName("勤怠管理Repositoryを呼出し、正常に登録できた場合、結果コード（正常）を返却すること")
    void createAttendanceInfo() {

        // setup
        Date date = new Date();
        AttendanceInfoRequest requestStub = this.createRequestCreateStub(date);
        AttendanceInfoResponse responseStub = this.createResponseCreateStub(date);
        doReturn(responseStub).when(this.repository).create(requestStub);

        // execute
        AttendanceInfoResponse actual = this.target.operateAttendanceInfo(requestStub);

        // assert
        assertEquals(responseStub, actual);
    }

    @Test
    @DisplayName("勤怠管理Repositoryを呼出し、正常に参照（日）できた場合、結果コード（正常）を返却すること")
    void readAttendanceInfo() {

        // setup
        Date date = new Date();
        AttendanceInfoRequest requestStub = this.createRequestReadDairyStub(date);
        AttendanceInfoResponse responseStub = this.createResponseReadStub(date);
        doReturn(responseStub).when(this.repository).read(requestStub);

        // execute
        AttendanceInfoResponse actual = this.target.operateAttendanceInfo(requestStub);

        // assert
        assertEquals(responseStub, actual);
    }

    @Test
    @DisplayName("勤怠管理Repositoryを呼出し、正常に参照（月）できた場合、結果コード（正常）を返却すること")
    void readMonthlyAttendanceInfo() {

        // setup
        Date now = new Date();
        AttendanceInfoRequest requestStub = this.createRequestReadMonthlyStub(now);
        AttendanceInfoResponse responseStub = this.createResponseReadStub(now);
        doReturn(responseStub).when(this.repository).readMonthly(requestStub);

        // execute
        AttendanceInfoResponse actual = this.target.operateAttendanceInfo(requestStub);

        // assert
        assertEquals(responseStub, actual);
    }

    @Test
    @DisplayName("勤怠管理Repositoryを呼出し、正常に更新できた場合、結果コード（正常）を返却すること")
    void updateAttendanceInfoSuccessTest() {

        // setup
        Date now = new Date();
        AttendanceInfoRequest requestStub = this.createRequestUpdateStub(now);
        AttendanceInfoResponse responseStub = this.createResponseUpdateStub(now);
        doReturn(responseStub).when(this.repository).update(requestStub);

        // execute
        AttendanceInfoResponse actual = this.target.operateAttendanceInfo(requestStub);

        // assert
        assertEquals(responseStub, actual);
    }

    /**
     * 勤怠情報リクエスト（登録）スタブ生成。
     * 
     * @param date 日付
     * @return 勤怠情報リクエスト（登録）スタブ
     */
    private AttendanceInfoRequest createRequestCreateStub(Date date) {
        return new AttendanceInfoRequest(
            CREATE,
            this.createAttendanceInfoCreateListStub(date)
        );
    }

    /**
     * 勤怠情報（登録）リストスタブ生成。
     * 
     * @param date 日付
     * @return 勤怠情報（登録）リストスタブ
     */
    private List<AttendanceInfo> createAttendanceInfoCreateListStub(Date date) {
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
        attendanceInfoList.add(
            new AttendanceInfo(
                "000000",
                date,
                PUNCH_IN,
                null,
                date,
                null,
                0,
                null
            )
        );
        return attendanceInfoList;
    }

    /**
     * 勤怠情報レスポンス（登録）スタブ生成。
     * 
     * @param date 日付
     * @return 勤怠情報レスポンス（登録）スタブ
     */
    private AttendanceInfoResponse createResponseCreateStub(Date date) {
        return new AttendanceInfoResponse(
            new CommonDto(new ResultDto(SUCCESS)),
            this.createAttendanceInfoCreateListStub(date)
        );
    }

    /**
     * 勤怠情報リクエスト（参照（日））スタブ生成。
     * 
     * @param date 日付
     * @return 勤怠情報リクエスト（参照（日））スタブ
     */
    private AttendanceInfoRequest createRequestReadDairyStub(Date date) {
        return new AttendanceInfoRequest(
            READ_DAILY,
            this.createAttendanceInfoReadListReqStub(date)
        );
    }

    /**
     * 勤怠情報（参照）リストリクエストスタブ生成。
     * 
     * @param date 日付
     * @return 勤怠情報（参照）リストリクエストスタブ
     */
    private List<AttendanceInfo> createAttendanceInfoReadListReqStub(Date date) {
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
        attendanceInfoList.add(
            new AttendanceInfo(
                "000000",
                date,
                null,
                null,
                null,
                null,
                0,
                null
            )
        );
        return attendanceInfoList;
    }

    /**
     * 勤怠情報レスポンス（参照（日））スタブ生成。
     * 
     * @param date 日付
     * @return 勤怠情報レスポンス（参照（日））スタブ
     */
    private AttendanceInfoResponse createResponseReadStub(Date date) {
        return new AttendanceInfoResponse(
            new CommonDto(new ResultDto(SUCCESS)),
            this.createAttendanceInfoReadListResStub(date)
        );
    }

    /**
     * 勤怠情報（参照）リストレスポンススタブ生成。
     * 
     * @param date 日付
     * @return 勤怠情報（参照）リストレスポンススタブ生成。
     */
    private List<AttendanceInfo> createAttendanceInfoReadListResStub(Date date) {
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
        attendanceInfoList.add(
            new AttendanceInfo(
                "000000",
                date,
                PUNCH_IN,
                null,
                date,
                null,
                0,
                null
            )
        );
        return attendanceInfoList;
    }

    /**
     * 勤怠情報リクエスト（参照（月））スタブ生成。
     * 
     * @param date 日付
     * @return 勤怠情報リクエスト（参照（月））スタブ
     */
    private AttendanceInfoRequest createRequestReadMonthlyStub(Date date) {
        return new AttendanceInfoRequest(
            READ_MONTHLY,
            this.createAttendanceInfoReadListReqStub(date)
        );
    }

    /**
     * 勤怠情報リクエスト（更新）スタブ生成。
     * 
     * @param date 日付
     * @return 勤怠情報リクエスト（更新）スタブ
     */
    private AttendanceInfoRequest createRequestUpdateStub(Date date) {
        return new AttendanceInfoRequest(
            UPDATE,
            this.createAttendanceInfoUpdateListStub(date)
        );
    }

    /**
     * 勤怠情報（更新）リストスタブ生成。
     * 
     * @param date 日付
     * @return 勤怠情報（更新）リストスタブ
     */
    private List<AttendanceInfo> createAttendanceInfoUpdateListStub(Date date) {
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
        attendanceInfoList.add(
            new AttendanceInfo(
                "000000",
                date,
                PUNCH_OUT,
                null,
                null,
                date,
                60,
                null
            )
        );
        return attendanceInfoList;
    }

    /**
     * 勤怠情報レスポンス（登録）スタブ生成。
     * 
     * @param date 日付
     * @return 勤怠情報レスポンス（登録）スタブ
     */
    private AttendanceInfoResponse createResponseUpdateStub(Date date) {
        return new AttendanceInfoResponse(
            new CommonDto(new ResultDto(SUCCESS)),
            this.createAttendanceInfoUpdateListStub(date)
        );
    }
}