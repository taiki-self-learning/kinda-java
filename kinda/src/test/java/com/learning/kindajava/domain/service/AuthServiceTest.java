package com.learning.kindajava.domain.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import com.learning.kindajava.common.dto.CommonDto;
import com.learning.kindajava.domain.model.AuthInfoRequest;
import com.learning.kindajava.domain.model.AuthInfoResponse;
import com.learning.kindajava.domain.repository.AuthRepository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class AuthServiceTest {

    @InjectMocks
    private AuthService target;

    @Mock
    private AuthRepository repository;

    @Test
    @DisplayName("AuthRepository.doLoginの結果を返却すること")
    void doLoginTest() {

        // setup
        doReturn(this.createAuthInfoResponseStub())
            .when(this.repository).doLogin(this.createAuthInfoRequestStub());

        // execute
        AuthInfoResponse actual = this.target.doLogin(this.createAuthInfoRequestStub());

        // assert
        assertEquals(actual, this.createAuthInfoResponseStub());
    }
    
    /**
     * AuthInfoRequest生成。
     * 
     * @return AuthInfoRequest
     */
    private AuthInfoRequest createAuthInfoRequestStub() {
        return new AuthInfoRequest("000000", "testPass");
    }

    /**
     * AuthInfoResponse生成。
     * 
     * @return AuthInfoResponse
     */
    private AuthInfoResponse createAuthInfoResponseStub() {
        return new AuthInfoResponse(new CommonDto(), "000000", "testUser", "ROLE_USER");
    }
}