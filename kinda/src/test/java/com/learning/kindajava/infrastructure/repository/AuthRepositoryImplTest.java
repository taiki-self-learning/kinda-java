package com.learning.kindajava.infrastructure.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import com.learning.kindajava.common.dto.CommonDto;
import com.learning.kindajava.domain.model.AuthInfoRequest;
import com.learning.kindajava.domain.model.AuthInfoResponse;
import com.learning.kindajava.infrastructure.client.LoginClient;
import com.learning.kindajava.infrastructure.dto.AuthRequestParam;
import com.learning.kindajava.infrastructure.dto.AuthResponseParam;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class AuthRepositoryImplTest {

    @InjectMocks
    private AuthRepositoryImpl target;

    @Mock
    private LoginClient client;

    @Test
    @DisplayName("LoginClient.executeの結果をAuthInfoResponseにマッピングし返却する")
    void doLoginTest() {

        // setup
        doReturn(this.createAuthResponseParamStub())
            .when(this.client).execute(this.createAuthRequestParamStub());

        // execute
        AuthInfoResponse actual = this.target.doLogin(this.createAuthInfoRequestStub());

        // assert
        assertEquals(actual, this.createAuthInfoResponseStub());

    }

    /**
     * AuthInfoRequest生成。
     * 
     * @return AuthInfoRequest
     */
    private AuthInfoRequest createAuthInfoRequestStub() {
        return new AuthInfoRequest("000000", "testPass");
    }

    /**
     * AuthRequestParam生成。
     * 
     * @return AuthRequestParam
     */
    private AuthRequestParam createAuthRequestParamStub() {
        return new AuthRequestParam("000000", "testPass");
    }

    /**
     * AuthResponseParam生成。
     * 
     * @return AuthResponseParam
     */
    private AuthResponseParam createAuthResponseParamStub() {
        return new AuthResponseParam(new CommonDto(), "000000", "testUser", "ROLE_USER");
    }

    /**
     * AuthInfoResponse生成。
     * 
     * @return AuthInfoResponse
     */
    private AuthInfoResponse createAuthInfoResponseStub() {
        return new AuthInfoResponse(new CommonDto(), "000000", "testUser", "ROLE_USER");
    }
}