package com.learning.kindajava.infrastructure.repository;

import static com.learning.kindajava.common.Constant.AttendanceCd;
import static com.learning.kindajava.common.Constant.DateFormat;
import static com.learning.kindajava.common.Constant.OperateDiv;
import static com.learning.kindajava.common.Constant.ResultCd;
import static com.learning.kindajava.common.util.Utility.string2Date;
import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.learning.kindajava.common.dto.CommonDto;
import com.learning.kindajava.common.dto.ResultDto;
import com.learning.kindajava.domain.model.AttendanceInfo;
import com.learning.kindajava.domain.model.AttendanceInfoRequest;
import com.learning.kindajava.domain.model.AttendanceInfoResponse;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@TestExecutionListeners({
    DependencyInjectionTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class
})
@Transactional
public class AttendanceMngRepositoryImplTest {

    @Autowired
    private AttendanceMngRepositoryImpl target;

    @Test
    @DatabaseSetup("/dbunit/attendancemng.ok.punchin.xml")
    @DisplayName("勤怠管理テーブル参照（日）を行い勤怠情報レスポンスを返却すること")
    void readTest() throws ParseException {

        // setup
        AttendanceInfoResponse expected =
            this.createAttendanceInfoResponseStub(OperateDiv.READ_DAILY);

        // execute
        AttendanceInfoResponse actual =
            this.target.read(this.createAttendanceInfoRequestStub(OperateDiv.READ_DAILY));
        
        // assert
        assertEquals(expected, actual);
    }

    @Test
    @DatabaseSetup("/dbunit/attendancemng.ok.monthly.xml")
    @DisplayName("勤怠管理テーブル参照（月）を行い勤怠情報レスポンスを返却すること")
    void readMonthlyTest() throws ParseException {

        // setup
        AttendanceInfoResponse expected =
            this.createAttendanceInfoResponseStub(OperateDiv.READ_MONTHLY);

        // execute
        AttendanceInfoResponse actual =
            this.target.readMonthly(this.createAttendanceInfoRequestStub(OperateDiv.READ_MONTHLY));
        
        // assert
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("勤怠管理テーブル登録を行い勤怠情報レスポンスを返却すること")
    void createTest() throws ParseException {

        // setup
        AttendanceInfoResponse expected =
            this.createAttendanceInfoResponseStub(OperateDiv.CREATE);

        // execute
        AttendanceInfoResponse actual =
            this.target.create(this.createAttendanceInfoRequestStub(OperateDiv.CREATE));

        // assert
        assertEquals(expected, actual);
    }

    @Test
    @DatabaseSetup("/dbunit/attendancemng.ok.punchin.xml")
    @DisplayName("勤怠管理テーブル登録を行い一意制約エラーの場合異常コードが返却されること")
    void createFailureTest() throws ParseException {

        // setup
        AttendanceInfoResponse expected =
            new AttendanceInfoResponse(
                new CommonDto(new ResultDto(ResultCd.ERROR)),
                new ArrayList<AttendanceInfo>()
            );

        // execute
        AttendanceInfoResponse actual =
            this.target.create(this.createAttendanceInfoRequestStub(OperateDiv.CREATE));

        // assert
        assertEquals(expected, actual);
    }

    @Test
    @DatabaseSetup("/dbunit/attendancemng.ok.punchin.xml")
    @DisplayName("勤怠管理テーブル更新を行い勤怠情報レスポンスを返却すること")
    void updateTest() throws ParseException {

        // setup
        AttendanceInfoResponse expected =
            this.createAttendanceInfoResponseStub(OperateDiv.UPDATE);

        // execute
        AttendanceInfoResponse actual =
            this.target.update(this.createAttendanceInfoRequestStub(OperateDiv.UPDATE));

        // assert
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("勤怠管理テーブル更新を行い更新対象が存在しない場合異常コードが返却されること")
    void updateFailureTest() throws ParseException {

        // setup
        AttendanceInfoResponse expected =
            new AttendanceInfoResponse(
                new CommonDto(new ResultDto(ResultCd.ERROR)),
                new ArrayList<AttendanceInfo>()
            );

        // execute
        AttendanceInfoResponse actual =
            this.target.update(this.createAttendanceInfoRequestStub(OperateDiv.UPDATE));

        // assert
        assertEquals(expected, actual);
    }

    /**
     * 勤怠情報リクエストスタブ生成。
     * 
     * @param operateDiv DB操作区分コード
     * @return 勤怠情報リクエストスタブ
     * @throws ParseException
     */
    private AttendanceInfoRequest createAttendanceInfoRequestStub(String operateDiv)
        throws ParseException {
        if (OperateDiv.READ_DAILY.equals(operateDiv)) {
            return new AttendanceInfoRequest(
                OperateDiv.READ_DAILY,
                this.createAttendanceInfoReqListReadStub()
            );
        } else if (OperateDiv.READ_MONTHLY.equals(operateDiv)) {
            return new AttendanceInfoRequest(
                OperateDiv.READ_MONTHLY,
                this.createAttendanceInfoReqListReadStub()
            );
        } else if (OperateDiv.CREATE.equals(operateDiv)) {
            return new AttendanceInfoRequest(
                OperateDiv.CREATE,
                this.createAttendanceInfoListCreateStub()
            );
        } else if (OperateDiv.UPDATE.equals(operateDiv)) {
            return new AttendanceInfoRequest(
                OperateDiv.UPDATE,
                this.createAttendanceInfoListUpdateStub()
            );
        } else {
            return new AttendanceInfoRequest(
                OperateDiv.DELETE,
                this.createAttendanceInfoReqListDeleteStub()
            );
        }
    }

    /**
     * 勤怠情報リクエスト用勤怠情報リストスタブ（参照）生成。
     * 
     * @return 勤怠情報リクエスト用勤怠情報リストスタブ（参照）
     * @throws ParseException
     */
    private List<AttendanceInfo> createAttendanceInfoReqListReadStub() throws ParseException {
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
        attendanceInfoList.add(
            new AttendanceInfo(
                "000000",
                string2Date("2020-12-30", DateFormat.YYYYMMDD_HYPHEN),
                null,
                null,
                null,
                null,
                0,
                null
            )
        );
        return attendanceInfoList;
    }

    /**
     * 勤怠情報リストスタブ（登録）生成。
     * 
     * @return 勤怠情報リストスタブ（登録）
     * @throws ParseException
     */
    private List<AttendanceInfo> createAttendanceInfoListCreateStub() throws ParseException {
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
        attendanceInfoList.add(
            new AttendanceInfo(
                "000000",
                string2Date("2020-12-30", DateFormat.YYYYMMDD_HYPHEN),
                AttendanceCd.PUNCH_IN,
                null,
                string2Date("10:00:00", DateFormat.HHMMSS),
                null,
                0,
                null
            )
        );
        return attendanceInfoList;
    }

    /**
     * 勤怠情報リストスタブ（登録）生成。
     * 
     * @return 勤怠情報リストスタブ（登録）
     * @throws ParseException
     */
    private List<AttendanceInfo> createAttendanceInfoListUpdateStub() throws ParseException {
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
        attendanceInfoList.add(
            new AttendanceInfo(
                "000000",
                string2Date("2020-12-30", DateFormat.YYYYMMDD_HYPHEN),
                AttendanceCd.PUNCH_OUT,
                null,
                string2Date("10:00:00", DateFormat.HHMMSS),
                string2Date("19:00:00", DateFormat.HHMMSS),
                60,
                null
            )
        );
        return attendanceInfoList;
    }

    /**
     * 勤怠情報リクエスト用勤怠情報リストスタブ（削除）生成。
     * 
     * @return 勤怠情報リクエスト用勤怠情報リストスタブ（削除）
     */
    private List<AttendanceInfo> createAttendanceInfoReqListDeleteStub() {
        // TODO:delete処理記載
        return null;
    }

    /**
     * 勤怠情報リクエストスタブ生成。
     * 
     * @param operateDiv DB操作区分コード
     * @return 勤怠情報リクエストスタブ
     * @throws ParseException
     */
    private AttendanceInfoResponse createAttendanceInfoResponseStub(String operateDiv)
        throws ParseException {
        if (OperateDiv.READ_DAILY.equals(operateDiv)) {
            return new AttendanceInfoResponse(
                new CommonDto(new ResultDto(ResultCd.SUCCESS)),
                this.createAttendanceInfoResListReadStub()
            );
        } else if (OperateDiv.READ_MONTHLY.equals(operateDiv)) {
            return new AttendanceInfoResponse(
                new CommonDto(new ResultDto(ResultCd.SUCCESS)),
                this.createAttendanceInfoResListReadMonthlyStub()
            );
        } else if (OperateDiv.CREATE.equals(operateDiv)) {
            return new AttendanceInfoResponse(
                new CommonDto(new ResultDto(ResultCd.SUCCESS)),
                this.createAttendanceInfoListCreateStub()
            );
        } else if (OperateDiv.UPDATE.equals(operateDiv)) {
            return new AttendanceInfoResponse(
                new CommonDto(new ResultDto(ResultCd.SUCCESS)),
                this.createAttendanceInfoListUpdateStub()
            );
        } else {
            return new AttendanceInfoResponse(
                new CommonDto(new ResultDto(ResultCd.SUCCESS)),
                this.createAttendanceInfoResListDeleteStub()
            );
        }
    }

    /**
     * 勤怠情報レスポンス用勤怠情報リストスタブ（参照（日））生成。
     * 
     * @return 勤怠情報レスポンス用勤怠情報リストスタブ（参照（日））
     * @throws ParseException
     */
    private List<AttendanceInfo> createAttendanceInfoResListReadStub() throws ParseException {
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
        attendanceInfoList.add(
            new AttendanceInfo(
                "000000",
                string2Date("2020-12-30", DateFormat.YYYYMMDD_HYPHEN),
                AttendanceCd.PUNCH_IN,
                null,
                string2Date("10:00:00", DateFormat.HHMMSS),
                null,
                0,
                null
            )
        );
        return attendanceInfoList;
    }

    /**
     * 勤怠情報レスポンス用勤怠情報リストスタブ（参照（月））生成。
     * 
     * @return 勤怠情報レスポンス用勤怠情報リストスタブ（参照（月））
     * @throws ParseException
     */
    private List<AttendanceInfo> createAttendanceInfoResListReadMonthlyStub()
        throws ParseException {
        List<AttendanceInfo> attendanceInfoList = new ArrayList<>();
        attendanceInfoList.add(
            new AttendanceInfo(
                "000000",
                string2Date("2020-12-01", DateFormat.YYYYMMDD_HYPHEN),
                AttendanceCd.PUNCH_IN,
                null,
                string2Date("10:00:00", DateFormat.HHMMSS),
                null,
                0,
                null
            )
        );
        attendanceInfoList.add(
            new AttendanceInfo(
                "000000",
                string2Date("2020-12-02", DateFormat.YYYYMMDD_HYPHEN),
                AttendanceCd.PUNCH_OUT,
                null,
                string2Date("10:00:00", DateFormat.HHMMSS),
                string2Date("19:00:00", DateFormat.HHMMSS),
                60,
                null
            )
        );
        return attendanceInfoList;
    }

    /**
     * 勤怠情報レスポンス用勤怠情報リストスタブ（削除）生成。
     * 
     * @return 勤怠情報レスポンス用勤怠情報リストスタブ（削除）
     */
    private List<AttendanceInfo> createAttendanceInfoResListDeleteStub() {
        // TODO:delete処理記載
        return null;
    }
}
