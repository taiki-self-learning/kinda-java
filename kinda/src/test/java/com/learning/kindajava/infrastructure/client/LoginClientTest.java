package com.learning.kindajava.infrastructure.client;

import static com.learning.kindajava.common.Constant.ResultCd.SUCCESS;
import static com.learning.kindajava.common.Constant.ResultCd.WARNING;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.learning.kindajava.common.WiremockTestUtil;
import com.learning.kindajava.common.dto.CommonDto;
import com.learning.kindajava.common.dto.ResultDto;
import com.learning.kindajava.infrastructure.dto.AuthRequestParam;
import com.learning.kindajava.infrastructure.dto.AuthResponseParam;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LoginClientTest {

    @Autowired
    WiremockTestUtil wiremock;

    @Autowired
    private LoginClient target;

    @BeforeEach
    void setup() {
        wiremock.start();
    }

    @AfterEach
    void tearDown() {
        wiremock.stop();
    }

    @Test
    @DisplayName("認証成功時、AuthResponseParamに認証情報を設定し返却する")
    void executeSuccessTest() {

        // execute
        AuthResponseParam actual = this.target.execute(this.createAuthRequestParamSuccessStub());

        // assert
        assertEquals(actual, this.createAuthResponseParamSuccessStub());
    }

    @Test
    @DisplayName("認証失敗時、AuthResponseParamに結果コードのみ設定し返却する")
    void executeFailureTest() {

        // execute
        AuthResponseParam actual = this.target.execute(this.createAuthRequestParamFailureStub());

        // assert
        assertEquals(actual, this.createAuthResponseParamFailureStub());
    }

    /**
     * AuthRequestParam(認証成功)生成。
     * 
     * @return AuthRequestParam
     */
    private AuthRequestParam createAuthRequestParamSuccessStub() {
        return new AuthRequestParam("000000", "testPass");
    }

    /**
     * AuthRequestParam(認証失敗)生成。
     * 
     * @return AuthRequestParam
     */
    private AuthRequestParam createAuthRequestParamFailureStub() {
        return new AuthRequestParam("000000", "failurePass");
    }

    /**
     * AuthResponseParam(認証成功)生成。
     * 
     * @return AuthResponseParam
     */
    private AuthResponseParam createAuthResponseParamSuccessStub() {
        return new AuthResponseParam(
            new CommonDto(new ResultDto(SUCCESS)),
            "000000",
            "testUser",
            "ROLE_USER"
        );
    }

    /**
     * AuthResponseParam(認証失敗)生成。
     * 
     * @return AuthResponseParam
     */
    private AuthResponseParam createAuthResponseParamFailureStub() {
        return new AuthResponseParam(
            new CommonDto(new ResultDto(WARNING)),
            null,
            null,
            null
        );
    }
}