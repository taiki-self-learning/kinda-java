# KINDA

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [概要](#%E6%A6%82%E8%A6%81)
- [はじめに](#%E3%81%AF%E3%81%98%E3%82%81%E3%81%AB)
- [事前準備](#%E4%BA%8B%E5%89%8D%E6%BA%96%E5%82%99)
    - [プロジェクトクローン](#%E3%83%97%E3%83%AD%E3%82%B8%E3%82%A7%E3%82%AF%E3%83%88%E3%82%AF%E3%83%AD%E3%83%BC%E3%83%B3)
    - [Homebrewインストール](#homebrew%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB)
    - [PostgreSQLインストール](#postgresql%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB)
    - [ユーザ 及び データベース作成](#%E3%83%A6%E3%83%BC%E3%82%B6-%E5%8F%8A%E3%81%B3-%E3%83%87%E3%83%BC%E3%82%BF%E3%83%99%E3%83%BC%E3%82%B9%E4%BD%9C%E6%88%90)
    - [テーブル 及び データ作成](#%E3%83%86%E3%83%BC%E3%83%96%E3%83%AB-%E5%8F%8A%E3%81%B3-%E3%83%87%E3%83%BC%E3%82%BF%E4%BD%9C%E6%88%90)
    - [mevenインストール](#meven%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB)
    - [yarnインストール](#yarn%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB)
    - [node_modulesインストール](#node_modules%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB)
- [使い方](#%E4%BD%BF%E3%81%84%E6%96%B9)
    - [サーバ起動](#%E3%82%B5%E3%83%BC%E3%83%90%E8%B5%B7%E5%8B%95)
    - [画面表示](#%E7%94%BB%E9%9D%A2%E8%A1%A8%E7%A4%BA)
    - [機能単体テスト](#%E6%A9%9F%E8%83%BD%E5%8D%98%E4%BD%93%E3%83%86%E3%82%B9%E3%83%88)
    - [機能単体テスト（カバレッジ含む）](#%E6%A9%9F%E8%83%BD%E5%8D%98%E4%BD%93%E3%83%86%E3%82%B9%E3%83%88%E3%82%AB%E3%83%90%E3%83%AC%E3%83%83%E3%82%B8%E5%90%AB%E3%82%80)
    - [画面単体テスト](#%E7%94%BB%E9%9D%A2%E5%8D%98%E4%BD%93%E3%83%86%E3%82%B9%E3%83%88)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## 概要

勤怠システム「KINDA」。
本プロジェクトには下記機能が含まれています。
 - JWT認証 
 - 勤怠情報登録（現時刻・指定時刻）
 - 勤怠情報編集
 - 勤怠情報一覧表示（当月）
 - 認証用API（auth-rest-api）接続
 - 機能単体テスト機能（Junit）
 - 画面単体テスト機能（cypress）

## はじめに

本プロジェクトはMac OS XまたはLinuxで使用することを目的としています。
また、以下の手順はMac OS X専用です。

## 事前準備

### プロジェクトクローン

```bash
$ git clone https://gitlab.com/taiki-self-learning/kinda-java.git 
```

### Homebrewインストール

```bash
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"  
```

### PostgreSQLインストール

```bash
$ brew install postgresql 
$ brew services start postgresql  
```

### ユーザ 及び データベース作成

```bash
$ psql postgres 
postgres=# CREATE ROLE kinda LOGIN PASSWORD 'kinda';
postgres=# CREATE DATABASE kinda;
postgres=# CREATE ROLE test LOGIN PASSWORD 'test';
postgres=# CREATE DATABASE test;
postgres=# \q
```

### テーブル 及び データ作成

```bash
$ cd 本プロジェクトパス/kinda-e2e/sql
$ psql -U kinda 
kinda=# \i create_tb_kinda_attendance_mng.sql
kinda=# \i create_tb_kinda_mst_code_mng.sql
kinda=# \i tb_kinda_mst_code_mng_insert.sql 
kinda=# \q
$ psql -U test 
test=# \i create_tb_kinda_attendance_mng.sql
test=# \i create_tb_kinda_mst_code_mng.sql
test=# \i tb_kinda_mst_code_mng_insert.sql 
test=# \q
```

### mevenインストール

```bash
$ brew install maven
```

### yarnインストール

```bash
$ brew install yarn --ignore-dependencies
```

### node_modulesインストール

```bash
$ yarn install
```

## 使い方

### サーバ起動

```bash
$ cd 本プロジェクトパス/kinda-e2e
$ yarn start:ut
```

### 画面表示

サーバ起動後ブラウザにて下記URLに接続  

```
http://localhost:8080/kinda/login
```

|ログインID|パスワード|権限|
|---|---|---|
|000000|user |ユーザ|
|999999|admin|管理者|

### 機能単体テスト

```bash
$ cd 本プロジェクトパス/kinda-e2e
$ yarn junit:run
```

### 機能単体テスト（カバレッジ含む）

```bash
$ cd 本プロジェクトパス/kinda-e2e
$ yarn junit:report
```

### 画面単体テスト

サーバ起動後下記コマンド実施

```bash
$ cd 本プロジェクトパス/kinda-e2e
$ yarn e2e:run:all
```